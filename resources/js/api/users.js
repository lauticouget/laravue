import axios from 'axios';

export default {

    find(id) {

        return axios.get(`/api/users/${id}`);
    },
    search(name) {

        return axios.get(`/api/users/search/${name}`);

    },
    create(data){

        return axios.post(`/api/users`, data);
    },
    update(id, data) {

        return axios.put(`/api/users/${id}`, data);
    },
    delete(id) {

        return axios.delete(`/api/users/${id}`);
    }
};
