require('./bootstrap');

import Vue from 'vue'
import VueRouter from 'vue-router'
import Vuex from 'vuex'

Vue.use(VueRouter)

import App from '../views/components/App.vue'
import Hello from '../views/components/Hello'
import Home from '../views/components/Home'
import TestsIndex from '../views/components/TestsIndex.vue'
import UsersIndex from '../views/components/UsersIndex.vue'
import UsersCreate from '../views/components/UsersCreate.vue'
import UsersEdit from '../views/components/UsersEdit.vue'
import NotFound from '../views/components/NotFound.vue'

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/hello',
            name: 'hello',
            component: Hello,
        },
        {
            path: '/tests',
            name: 'tests.index',
            component: TestsIndex,
        },
        {
            path: '/users',
            name: 'users.index',
            component: UsersIndex,
        },
        {
            path: '/users/create',
            name: 'users.create',
            component: UsersCreate,
        },
        {
            path: '/users/:_id/edit',
            name: 'users.edit',
            component: UsersEdit,
        },
        { path: '/404', name: '404', component: NotFound },
        { path: '*', redirect: '/404' },
    ],
});

const app = new Vue({
    el: '#app',
    components: { App },
    router,
});
