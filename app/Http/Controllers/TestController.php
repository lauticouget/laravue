<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Test;

class TestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __invoke()
    {

        $tests = Test::all();

        return $tests;
    }
}
